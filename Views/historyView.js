import React, {Component} from 'react';

import {
        Button,
        Text, 
        View,
        Image,
        Linking,
        TouchableHighlight
      } from 'react-native';

      import DatePicker from 'react-native-datepicker'
import { Settings } from '../settings';

import {HistorySheet, styles} from '../styleSheets.js'

export class HistoryView extends Component<Props>
{
    constructor(props)
    {
        super(props);

        this.state ={
            fromDate:new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate(),
            fromTime:"00:00",
            toDate:new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate(),
            toTime:new Date().getHours() + ":" +  new Date().getMinutes()
        };
    }

    componentDidMount()
    {

    }


    parseDate(date,time)
    {
       date = String(date);
        date = date.split('-');
        let date1 = new Date();
        
        date1 = new Date(date1.setFullYear(date[0],date[1]-1,date[2]));
        
        time = String(time).split(':');
        
        date1 = new Date(date1.setHours(parseInt(time[0]),parseInt(time[1])));
        return date1;
    }


render()
{

    return(
        <View>
        <View>
            <View style={{alignSelf:"center",justifyContent:"center", flex:1,flexDirection:"row"}}>
        <Text style={HistorySheet.DatePicker}>Od:</Text><DatePicker date={this.state.fromDate} onDateChange={(date) => {this.state.fromDate = date; this.forceUpdate() }}  /><DatePicker mode="time" onDateChange={(date) => {this.state.fromTime = date ; this.forceUpdate()}} date={this.state.fromTime} />
            </View>
            <View style={{alignSelf:"center",justifyContent:"center",flex:1,flexDirection:"row"}}>
            <Text style={HistorySheet.DatePicker} >Do:</Text><DatePicker date={this.state.toDate} onDateChange={(date) => {this.state.toDate = date; this.forceUpdate()}} /><DatePicker mode="time" date={this.state.toTime}  onDateChange={(date) => {this.state.toTime = date ; this.forceUpdate()}} />
            </View>
            <Button title="Filtruj" onPress = {()=>{this.HistoryElement.refresh()}} />
        </View>
        <View>
             <HistoryElementHandler scrollViewBackToTop={this.props.scrollViewBackToTop} onRef = {ref=>(this.HistoryElement = ref)} Database={this.props.Database} from={this.parseDate(this.state.fromDate,this.state.fromTime)} to={this.parseDate(this.state.toDate,this.state.toTime)} /> 
        </View>
        </View>
    );
}
}

export class HistoryElementHandler extends Component<Props>
{

      componentWillUnmount() {
        this.props.onRef(undefined)
      }

    constructor(props)
    {
        super(props);
        this.state = {
            array:[],
            page:1,
            offset:0,
        }
    }

    refresh(newSearch = true)
    {
        if(newSearch)
        {
        this.state.offset = 0;
        this.state.page = 1;
        }
        this.componentDidMount();
        //this.forceUpdate();
    }
    componentDidMount()
    {
        
        this.props.onRef(this);
        this.state.array = new Array();
        
        this.props.Database.showRecords(this.props.from,this.props.to,(res)=>{
            let a = new Array();
            for(let i = 1; i < res.rows.length;i++)
            {
                a.push(res.rows.item(i));
            }

            let index = 1;
            
            a.forEach((element)=>{ this.state.array.push((<HistoryElement key={index++} style = {{flex:1}}  data={element} />)); });
            this.forceUpdate();
        },
        Settings.historyView.PageAmount,
        this.state.offset
        );
    }
    render()
    {
        
        return(
            
            <View>
                {this.state.array}
                <View style={{flexDirection:"row",alignSelf:"center",justifyContent:"center", flex:1}}>
                    <Button title="<-"  onPress={()=>{
                        if(this.state.page>1){
                        --this.state.page;
                        this.state.offset-=Settings.historyView.PageAmount;
                        this.props.scrollV
                        this.props.scrollViewBackToTop();
                        this.refresh(false);
                        
                        }
                        else
                        {
                        return
                        }
                        }}/>

                    <Text style={{paddingLeft:10,paddingRight:10,textAlignVertical:"center",fontSize:17,backgroundColor:"#ccc",color:"#000"}}>  {this.state.page}  </Text>
                    
                    <Button  title="->"
                    
                     onPress={()=>{
                        if(this.state.array.length == Settings.historyView.PageAmount-1){
                        ++this.state.page;
                        this.state.offset+=Settings.historyView.PageAmount;
                        this.props.scrollViewBackToTop();
                        this.refresh(false);
                        }
                        else{
                        return
                        }
                        }}/>
                </View>
            </View>
        )
    }
}

export class HistoryElement extends Component<Props>
{
addZero(var1)
{
    if(var1<10)
    {
        return "0" + var1;
    }
    else return var1;
}
formatDateString(datas)
{
    let final = "";

    if(datas.getHours() < 10)
    {
        final += " " + datas.getHours();
    }
    else
    {
        final += datas.getHours();
    }
    final += ":";
    final += this.addZero(datas.getMinutes());
    final += ":";
    final += this.addZero(datas.getSeconds());
    final += " ";
    final += this.addZero(datas.getDate());
    final += "-";
    final += this.addZero(datas.getMonth()+1);
    final += "-";
    final += datas.getFullYear() + "  ";
    return final;
}
   
render()
{
    let datas = new Date(this.props.data.data);
    let dataw = this.formatDateString(datas);
    return(
        <View style = {HistorySheet.InfView}>
            <View >
                <View>
                    <View style={{flexDirection:"row"}}>
                        <Text style= {HistorySheet.InfTitle}>{dataw}</Text>
                        <TouchableHighlight style={{ marginLeft:15}}

                            onPress={()=>{Linking.openURL("geo:" + this.props.data.latitude+","+this.props.data.longitude+"?z=3") ;}}  style={{ marginTop:1, borderWidth:1}}
                            >
                        <View style={{flexDirection:"row"}}>
                            <Image style={styles.tinyImage} source={require("../images/location2.png")} />
                            <Text style={{ marginRight:5}}>Znajdź na mapie</Text>
                        </View>
                        </TouchableHighlight>
                    </View>
                    <View >
                        <Text style= {HistorySheet.InfText}>Szerokość: {this.props.data.latitude}</Text>
                        <Text style= {HistorySheet.InfText}>Długosc: {this.props.data.longitude}</Text>
                        <Text style= {HistorySheet.InfText}>Wysokość: {this.props.data.altitude}</Text>
                        <Text style= {HistorySheet.InfText}>Prędkość: {this.props.data.speed}</Text>
                    </View>
                </View>
            </View>
        </View>
    );
}
}

