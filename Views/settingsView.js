import React, {Component} from 'react';
import {
        Button,
        Alert,
        Text, 
        View,
        TextInput
      } from 'react-native';

import {ElementSheet} from '../styleSheets';
import { Settings } from '../settings';
import  CheckBox  from 'react-native-check-box';
import RNRestart from 'react-native-restart';




export class SettingsView extends Component<Props>
{
    constructor(Props)
    {
        super(Props);
        this.state = {isChecked:false};
        this.state = {c:[],save:true}

        this.state.c[0] = Settings.general.saveHistory;
        this.state.c[1] = Settings.general.backgroundTask;
        this.state.c[2] = Settings.database.deleteAfter;

    }

    

    componentWillUnmount()
    {
        if(this.state.save)
        Settings.save();
    }
    render()
    {
        let expireInDays = null;
        if(this.state.c[2]){
            expireInDays = (
            <View>
            <Text style = {{marginLeft:15}}>Ilość dni</Text>
            <DigitInput  Input = {Settings.database.expireInDays} onSubmit = {(text)=>{
              if(text == "" || text == null || parseInt(text)<0){
                 return; 
              }else{
              Settings.database.expireInDays = parseInt(text)}}}/>
            
            </View>
            );
        }
        
        return(
            <View >
                <Text style = {ElementSheet.InfTitle}>Ustawienia ogólne</Text>
                <CheckBox
                style={{flex: 1, padding: 10}}
                onClick={()=>{
                this.state.c[0] = !this.state.c[0];
                Settings.general.saveHistory = this.state.c[0];
                this.setState(this.state)
                }}
                isChecked={this.state.c[0]}
                leftText={"Zapis lokalizacji w bazie danych"}
                />

                <Text style = {ElementSheet.InfTitle}>Ustawienia zapisu</Text>
                
                <Text style = {{marginLeft:15}}>Odległość między wpisami do bazy danych (Metry)</Text>
                <DigitInput  Input = {Settings.geolocation.distanceToRecord} onSubmit = {async (text)=>{

                  Settings.geolocation.distanceToRecord = parseInt(text); Settings.save(); Settings.restartWatcher = true;} }/>
                  
                <CheckBox
                style={{flex: 1, padding: 10}}
                onClick={()=>{
                    if(!this.state.c[2]){
                    Alert.alert(
                        'UWAGA',
                        'Włączenie tej funkcji oraz ilości dni skutkuje usunięciem starszych wpisów.',
                        [
                          {text: 'Kontynuuj', onPress: () => {  
                            this.state.c[2] = !this.state.c[2];
                            Settings.database.deleteAfter = this.state.c[2];
                            this.setState(this.state)}
                        },
                          {text: 'Anuluj'},
                        ]
                      );
                    }
                      else
                      {
                      this.state.c[2] = !this.state.c[2];
                      Settings.database.deleteAfter = this.state.c[2];
                      this.setState(this.state);}
                    

                }}
                isChecked={this.state.c[2]}
                leftText={"Przechowuj historię przez określoną ilość dni"}
                />
                {expireInDays}

                <Text style = {{marginLeft:15}}>Ilość wpisów w historii na stronę</Text>
                <DigitInput  Input = {Settings.historyView.PageAmount - 1} onSubmit = {(text)=>{Settings.historyView.PageAmount = parseInt(text) + 1}}/>
                <View style={{margin:15}}>
                <Button style={{width:"90%",margin:5}} title="Wyczyść Historię" onPress={()=>{
                        Alert.alert(
                            'UWAGA',
                            'Kontynuowanie spowoduje wyczyszczenie bezpowrotny wszystkich wpisów. Czy chcesz kontynuować?',
                            [
                              {text: 'Kontynuuj', onPress: () => {  
                               this.props.Database.clear();
                            }},
                              {text: 'Anuluj'},
                            ]
                          );
                }}  />
                </View>
                <View style={{margin:15}}>
                <Button style={{width:"90%",margin:15}} title="Przywróć ustawienia domyślne" onPress={()=>{
                        Alert.alert(
                            'UWAGA',
                            'Kontynuowanie spowoduje powrót do oryginalnych ustawień. Czy chcesz kontynuować?',
                            [
                              {text: 'Kontynuuj', onPress: () => {  
                               Settings.toDefault();
                               Alert.alert("SUKCES","Ustawienia domyślne ustawione. Aplikacja zostanie zrestartowana");
                               this.state.save = false;
                               RNRestart.Restart();
                            }},
                              {text: 'Anuluj'},
                            ]
                          );
                }}  />
                
                </View>


                <Text style = {{marginLeft:15}}>Icons has been made by Gregor Cresnar</Text>
            </View>
        );
    }
}

class DigitInput extends Component<Props>
{
    constructor(Props)
    {
        super(Props);
        this.state = {text:null,error:false};


    }

    
    parse(text)
    {
        let reg = /^[0-9]*$/
        text=text.toLowerCase();

        if(!reg.test(text) || text == "")
        {
            this.setState({text: text,error:true})
            return false;
        }
        else
        {
          this.setState({text: text,error:false})
            return true;
        }
    }

    render()
    {
        return(
            <TextInput style = { {borderWidth:1,marginTop:5, marginBottom:5, marginLeft:15,marginRight:15,paddingHorizontal:20,backgroundColor:(this.state.error?"red":"lightgrey")}}
            keyboardType='numeric'
             maxLength={5} 
             value={this.state.text}
             defaultValue = {this.props.Input.toString()}
             onChangeText={(text)=>{
               if(this.parse(text))
             {
               this.props.onSubmit(text)
             }
               }}
             />
             
        )
    }
}
