import React, {Component} from 'react';

import {
        Text, 
        View
      } from 'react-native';

import { ElementSheet} from '../styleSheets.js'

import Geolocation from 'react-native-geolocation-service';

export class LocationView extends Component<Props>
{

    Interval;


    constructor(props)
    {
        super(props);
        this.state = {Coords:{latitude:0,longitude:0,altitude:0,speed:0}} 

    }

    componentWillUnmount()
    {
        clearInterval(this.Interval);
    }

    componentDidMount()
    {
        this.int();
    }

    int()
    {
       this.Interval = setInterval(()=>{
           Geolocation.getCurrentPosition((res)=>{
                this.Coords = res.coords;
                this.setState({Coords:res.coords});
           })
        },1000);
    }

    render()
    {
        
        return(
            <View style = {{margin:10,flex:1}}> 
                    <Text style={{margin:10}} >Szerokość geograficzna</Text>
                    <Text style={ElementSheet.InfTitle}>{this.state.Coords.latitude}</Text>
                    <Text style={{margin:10}}>Długość geograficzna</Text>
                    <Text style={ElementSheet.InfTitle} >{this.state.Coords.longitude}</Text>
                    <Text style={{margin:10}}>Wysokość geograficzna</Text>
                    <Text style={ElementSheet.InfTitle}>{this.state.Coords.altitude}</Text>
                    <Text style={{margin:10}}>Prędkość</Text>
                    <Text style={ElementSheet.InfTitle}>{this.state.Coords.speed}</Text>
            </View>
        );
    }
}