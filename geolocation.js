
import Geolocation from 'react-native-geolocation-service';
import {PermissionsAndroid} from 'react-native';
import { Settings } from './settings';

export class GeoLocHandler
{
    Database=undefined;
    watch= undefined;
    informationCreator=undefined;

    AddCurrentLocation()
    {
      Geolocation.getCurrentPosition((res)=>{
        this.Database.addRecord(res.coords)
      });
    }
    
    async requestPermission() {
    try {

      const granted = await PermissionsAndroid.request(
        'android.permission.ACCESS_FINE_LOCATION',
        {
          title: 'Lokalizacja',
          message:
            'Aby aplikacja działała konieczna jest zgoda na użycie lokalizacji ',
            buttonNegative: 'Nie zgadzam się',
            buttonPositive: 'Zgadzam się'
        },
      );

    if(granted === PermissionsAndroid.RESULTS.GRANTED){
        return true
    }
    else{
        return false
    }


    } catch (err) {
      console.error(err);
    }
  }


    async startWatch(){

        
        if(!await this.requestPermission())
        {
            return;
        }

        Geolocation.requestAuthorization();



        watch = Geolocation.watchPosition(
            (location) => 
            {
                
                if(Settings.general.saveHistory)
                {
                     this.Database.addRecord(location.coords);
                }
            }
            ,
            (errors) => {
                alert("Error:" + errors.message);
            }
            ,
            {
                distanceFilter: Settings.geolocation.distanceToRecord,
                enableHighAccuracy: true
            }
        );
    }


    async clearWatch(){
    Geolocation.stopObserving();
    }

}