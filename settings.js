import {AsyncStorage} from 'react-native';

export class Settings
{
     static save()
     {
        AsyncStorage.setItem("settings_general",JSON.stringify(this.general));
        AsyncStorage.setItem("settings_database",JSON.stringify(this.database));
        AsyncStorage.setItem("settings_history",JSON.stringify(this.historyView));
        AsyncStorage.setItem("settings_geolocation",JSON.stringify(this.geolocation));
     }

    static toDefault()
    {
         AsyncStorage.setItem("settings_general","");
         AsyncStorage.setItem("settings_database","");
         AsyncStorage.setItem("settings_history","");
         AsyncStorage.setItem("settings_geolocation","");
        
        this.load();
    }



    static load()
    {
        let x = AsyncStorage.getItem("settings_general");
        x.finally((res)=>{
            if(res!=null)
                this.general = JSON.parse(res);
        });

        x = AsyncStorage.getItem("settings_database");
        x.finally((res)=>{
            if(res!=null)
                this.database = JSON.parse(res);
        });

        x = AsyncStorage.getItem("settings_history");
        x.finally((res)=>{
            if(res!=null)
                this.historyView = JSON.parse(res);
        });

        x = AsyncStorage.getItem("settings_geolocation");
        x.finally((res)=>{
            if(res!=null)
                this.geolocation = JSON.parse(res);
        });


    }

    static general = {
        watchPosition: true,
        saveHistory: true,
        backgroundTask:false
    }
    static database = {
        deleteAfter: false,
        expireInDays: 0,
        
    }

    static historyView = {
        PageAmount:11,
    }

    static geolocation = {
        distanceToRecord:1000
    }

    static restartWatcher = false;

}