/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */


import React, {Component} from 'react';
import {ScrollView,
        TouchableHighlight,
        Image,
        Button,
        Text, 
        View
        
      } from 'react-native';
import {ElementSheet,styles} from './styleSheets';

import {Settings} from './settings.js';

Settings.load();

import {DataBase} from './dbhistory.js';
import {GeoLocHandler} from './geolocation.js';

type Props = {};

import {SettingsView} from './Views/settingsView.js';
import {HistoryView} from './Views/historyView.js';
import {LocationView} from './Views/currentLocationView.js';

// const instructions = Platform.select({
//   ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
//   android:
//     'Double tap R on your keyboard to reload,\n' +
//     'Shake or press menu button for dev menu',
// });



export default class App extends Component<Props> {
  Db;
  GeoLocHandler;

  constructor(props)
  {

    super(props)
    this.state = {SelectedId:1}

    this.Db = new DataBase();
    this.Db.open();
    


   this.GeoLocHandler = new GeoLocHandler();
   this.GeoLocHandler.Database = this.Db;
   
   this.mainScrollView = React.createRef();
   this.GeoLocHandler.startWatch();


  }

  render() {

    if(Settings.database.deleteAfter)
    {
      this.Db.deleteOlderThan(Settings.database.expireInDays);
    }

    if(Settings.restartWatcher)
    {
        this.GeoLocHandler.clearWatch();
        this.GeoLocHandler.startWatch();
        Settings.restartWatcher = false;
    }

    let Content;
    switch(this.state.SelectedId)
    {
    case 1:
        Content = (
          <LocationView />
            )
      break;
      case 2:
          Content = (
            <HistoryView scrollViewBackToTop={()=>{this.mainScrollView.scrollTo({x:0,y:0,animated:true,duration:1})}} Database={this.Db} />
            )
        break;
        case 3:
            Content = (
            <View style={styles.container}>
              <Text></Text>
               
                </View>)
          break;
          case 4:
              Content = (
              <View style={styles.container} >
                <SettingsView Database={this.Db} />
                </View>)
            break;
    } 

    return (
      <View style={styles.container}>
        <ScrollView ref={(ref)=>{this.mainScrollView=ref}} style={styles.container}>

        {Content}
        
       </ScrollView>
       <InformationHandler onRef={ref=>(this.infHand = ref)}/>
       <Toolbar onSelectedID={(Id)=>{this.setState({ SelectedId:Id });}}  onRef={(ref)=>(this.Toolbar = ref)} />
      </View>
    );
  }
}

export class InformationHandler extends Component<Props>
{

  componentDidMount() {
    this.props.onRef(this)
  }

  componentWillUnmount() {
    this.props.onRef(undefined)
  }
  constructor(props)
  {
    super(props);
    this.state = {storage:[]}
  }

  createMessage(titles,msgs)
  {
    this.state.storage.push({msg:msgs,title:titles});
    this.forceUpdate();
  }
 
    render()
    {

      const Informations = this.state.storage.map(
        (value,key)=>{return(<Information style = {{flex:1}} key = {key} name = {value.title} value = {value.msg} />);
          }
        );
      return(
        <View style={{width:"100%"}}>
        {Informations}
        </View>
      );
    }
}

export class Information extends Component<Props>
{
  constructor(props)
  {
    super(props);
    this.state = {show: true};

  }

  render(){
    if(this.state.show)
      return (
          <View style = {ElementSheet.InfView}>
                <View style = {{flexDirection:"row"}}>
                  <Image style={styles.Image} source={require("./images/warning.png")}/>
                  <View>
                  <Text style= {ElementSheet.InfTitle}>{this.props.name}</Text>
                  <View style={{width:"90%"}}>
                  <Text style= {{padding:5}}>{this.props.value}</Text>
                  </View>
                  <Button style={{fontSize:12, flex:1}} onPress = {() => { this.setState({show: false});}} title = "Rozumiem"/>
                  </View>
                </View>
          </View>
      );
      else
       return (null);
  }
}

export class Toolbar extends Component<Props>{

  componentDidMount() {
    this.props.onRef(this)
  }

  componentWillUnmount() {
    this.props.onRef(undefined)
  }

  state = {selected:0}
  render() {
    return (
        <View style={styles.toolbar}>
        <View  style={{ justifyContent:"center", flexDirection:"row"}}>
          <TouchableHighlight  onPress = {()=>{this.props.onSelectedID(1)}}><Image style = {styles.Image} source = {require("./images/location.png")} /></TouchableHighlight>
          <TouchableHighlight  onPress = {()=>{this.props.onSelectedID(2)}}><Image style = {styles.Image} source = {require("./images/history.png")} /></TouchableHighlight>
          {/* <TouchableHighlight  onPress =  {()=>{this.props.onSelectedID(3)}}><Image style = {styles.Image} source = {require("./images/notes.png")} /></TouchableHighlight> */}
          <TouchableHighlight  onPress =  {()=>{this.props.onSelectedID(4)}}><Image style = {styles.Image} source = {require("./images/settings.png")} /></TouchableHighlight>
       </View>
       </View>
    );
  }
}