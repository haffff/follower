import { openDatabase } from 'react-native-sqlite-storage';
import {Alert} from 'react-native';
export class DataBase{
    Db=undefined;
    open(){
        Db = openDatabase({ name: 'UserDatabase.db' },()=>{},(er)=>{alert("Baza danych nie mogła się otworzyć, błąd: " + er.message)});
        Db.transaction( (txn)=>{
             txn.executeSql(
                 "CREATE TABLE IF NOT EXISTS locations(id INTEGER PRIMARY KEY AUTOINCREMENT, data DATETIME, longitude FLOAT, latitude FLOAT, altitude FLOAT,speed FLOAT);"
              ,[]
              ,()=>{} ,(er)=>{alert("ERROR " + er.message);});
             })
    }
    

    addRecord(coordinates){

        Db.transaction( (txn)=>{
            
             txn.executeSql("INSERT INTO locations (data,longitude,latitude,altitude,speed) VALUES( ? , ?, ? , ?, ? )"
              ,
              [new Date(Date.now()).toISOString(),coordinates.longitude,coordinates.latitude,coordinates.altitude , coordinates.speed],
              ()=>{},(er)=>{alert("ERROR: " + er.message);}
              );
            });
    }

    showRecords(from,to,callbackFunction,howmuch = 10,offset = 0){
         if(to === undefined)
         {
             to = new Date(Date.now()).toISOString();
         }
         else
         to = new Date(to).toISOString();
         if(from === undefined)
         {
             from = new Date(Date.now()-new Date(0).setHours(24)).toISOString();
         }
         else
         from = new Date(from).toISOString();

        Db.transaction( (txn)=>{
            txn.executeSql("SELECT * FROM locations WHERE data BETWEEN ? AND ? ORDER BY data DESC LIMIT ? OFFSET ? ",
             [from,to,howmuch,offset],
            (tx,res)=>{
                callbackFunction(res);
             },(e)=>{alert(e.message)}
             )
           });
    
    
    }

    deleteOlderThan(days)
    {
        if(days == 0)
        {
            return;
        }
        let date = new Date(Date.now()- new Date(0).setDate(days+1));
        date = date.toISOString();
        Db.transaction( (txn)=>{
           
            txn.executeSql("DELETE FROM locations WHERE data < ?;",
             [date],null,(error)=>{alert("Error: "+error.message)})
           });
    }

    clear()
    {
        Db.transaction( (txn)=>{
           
            txn.executeSql("DELETE FROM locations;",
             [],
             ()=>{Alert.alert("Sukces","Pomyślnie wyczyszczono bazę danych")},(error)=>{alert("Error: "+error.message)})
           });
    }
        




    }











