
import {StyleSheet} from 'react-native';


export const HistorySheet = StyleSheet.create({
    DatePicker:
    {
    textAlignVertical:"center",
    fontSize:15
    },
    InfView:
    {
      borderWidth:1,
      borderStyle:"dashed",
      //flex:1,
      width:"100%",
      backgroundColor:"#FFF"
    },
    InfTitle:
    {
      marginLeft:10,
      fontSize:17,
      fontWeight:'bold'
    },
    InfText:
    {
    padding:2,
    marginLeft:5,

    }
    
    });

    
export const styles = StyleSheet.create({
    container: {
      flex: 1,
      //justifyContent: 'center',
      //alignItems: 'center',
      backgroundColor: 'white',
    },
    toolbar: {
      width:"100%",
      bottom:0,
      left:0,
      backgroundColor: 'lightgrey'
    } 
    ,Image:
    {
      margin:5,
      width:70,
      height:70
    }
    ,tinyImage:
    {
      margin:5,
      width:10,
      height:10
    }
  });
  
  
export const ElementSheet = StyleSheet.create({
    InfView:
    {
      borderWidth:1,
      borderStyle:"dashed",
      //flex:1,
      width:"100%",
      
      backgroundColor:"#c4c4c4"
    },
    InfTitle:
    {
      color:"#000",
      marginLeft:10,
      fontSize:17,
    }
    
    });